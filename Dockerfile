FROM ruby

WORKDIR /home/app

COPY ./ .

ENV PORT 3000

EXPOSE 3000

# install essentials
RUN apt-get -y update
RUN apt-get -y install build-essential git-core

# install rbenv
RUN git clone https://github.com/sstephenson/rbenv /usr/local/rbenv
RUN mkdir -p /usr/local/rbenv/plugins
RUN echo 'export RBENV_ROOT=/usr/local/rbenv' > /etc/profile.d/rbenv.sh
RUN echo 'export PATH=$RBENV_ROOT/bin:$PATH' >> /etc/profile.d/rbenv.sh
RUN echo 'eval "$(rbenv init -)"'

# install plugins
RUN git clone https://github.com/sstephenson/ruby-build /usr/local/rbenv/plugins/ruby-build
RUN git clone https://github.com/sstephenson/rbenv-vars /usr/local/rbenv/plugins/rbenv-vars
RUN git clone https://github.com/jf/rbenv-gemset /usr/local/rbenv/plugins/rbenv-gemset

RUN gem install - y rails bundler 
RUN gem install - y rails
RUN apt-get update && apt-get install -y nodejs

ENTRYPOINT [ "/bin/bash" ]

#RUN apt-get update -qq && apt-get install -y build-essential

# for postgres
#RUN apt-get install -y libpq-dev

# for nokogiri
#RUN apt-get install -y libxml2-dev libxslt1-dev

# for capybara-webkit
#RUN apt-get install -y libqtwebkit4

# for a JS runtime
#RUN apt-get install -y nodejs

#WORKDIR /var/www

#COPY ./ .

#EXPOSE 3000
##ENTRYPOINT ["rails", "server"]

